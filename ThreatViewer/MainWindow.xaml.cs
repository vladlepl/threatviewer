﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using OfficeOpenXml;

namespace ThreatViewer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private static string threatsUrl = @"https://bdu.fstec.ru/files/documents/thrlist.xlsx";
        private static string threatsPath = "thrlist.xlsx";

        private Pagination pagination;

        public MainWindow()
        {
            InitializeComponent();

            pagination = new Pagination();
            pagination.DisplayedThreatsChanged += UpdatePaginationGUI;
            ThreatsDG.ItemsSource = pagination.DisplayedThreats;

            FirstPageBtn.Click += delegate (object sender, RoutedEventArgs e) { pagination.MoveFirstPage(); };
            PreviousPageBtn.Click += delegate (object sender, RoutedEventArgs e) { pagination.MovePreviousPage(); };
            NextPageBtn.Click += delegate (object sender, RoutedEventArgs e) { pagination.MoveNextPage(); };
            LastPageBtn.Click += delegate (object sender, RoutedEventArgs e) { pagination.MoveLastPage(); };

            CloseWindowBtn.Click += delegate (object sender, RoutedEventArgs e) { this.Close(); };

            UpdatePaginationGUI();
        }

        private async void Window_Loaded(object sender, RoutedEventArgs e)
        {
            pagination.UpdateThreats(await GetThreatsAsync());

            UpdateBtn.IsEnabled = true;
        }

        private async Task<List<Threat>> GetThreatsAsync()
        {
            if (!File.Exists(threatsPath))
            {
                string message = "Файл с базой угроз не найден!\nПроизвести первичную загрузку?\n\nПримечание: Вы также можете выполнить загрузку самостоятельно, нажав на кнопку \"Обновить базу\" в главном окне.";
                if (MessageBox.Show(message, "Не удалось найти файл с базой угроз", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (!await Task.Run(new Func<bool>(() => DownloadFile(threatsUrl , threatsPath))))
                    {
                        MessageBox.Show("Ошибка при попытки скачивания базы угроз!\nПроверьте наличие подключения к сети Интернет и попробуйте снова!", "Не удалось скачать базу угроз!", MessageBoxButton.OK, MessageBoxImage.Error);
                        return new List<Threat>();
                    }
                }
                else
                    return new List<Threat>();
            }

            return await Task.Run(new Func<List<Threat>>(() => ParseThreats(threatsPath)));
        }

        private bool DownloadFile(string url, string path)
        {
            try
            {
                new WebClient().DownloadFile(url, path);
                return true;
            }
            catch
            {
                return false;
            }
        }

        private List<Threat> ParseThreats(string path)
        {
            try
            {
                return ReadThreatsFromXLS(path);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + $"\n\nПопробуйте обновить данные!\nЕсли это не поможет, Вам необходимо самостоятельно удалить файл {threatsPath}, находящийся в папке с программой!", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                return new List<Threat>();
            }
        }

        private static List<Threat> ReadThreatsFromXLS(string filePath)
        {
            using (var sr = new StreamReader(filePath))
            {
                using (var package = new ExcelPackage(sr.BaseStream))
                {
                    ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                    var worksheet = package.Workbook.Worksheets[0];
                    int colCount = worksheet.Dimension.End.Column;
                    int rowCount = worksheet.Dimension.End.Row;

                    var threats = new List<Threat>();

                    string errorMessage = "";

                    for (int row = 3; row <= rowCount; row++)
                    {
                        try
                        {
                            int id = int.Parse(worksheet.Cells[row, 1].Value?.ToString().Trim() ?? "0");
                            string name = worksheet.Cells[row, 2].Value?.ToString().Trim() ?? "";
                            string description = worksheet.Cells[row, 3].Value?.ToString().Trim() ?? "";

                            string sources = worksheet.Cells[row, 4].Value?.ToString().Trim() ?? "";
                            string objects = worksheet.Cells[row, 5].Value?.ToString().Trim() ?? "";

                            bool hasConfidentialityViolation = int.Parse(worksheet.Cells[row, 6].Value?.ToString().Trim() ?? "0") == 1;
                            bool hasAvailabilityViolation = int.Parse(worksheet.Cells[row, 7].Value?.ToString().Trim() ?? "0") == 1;
                            bool hasIntegrityViolation = int.Parse(worksheet.Cells[row, 8].Value?.ToString().Trim() ?? "0") == 1;

                            threats.Add(new Threat(id, name, description, sources, objects, hasConfidentialityViolation, hasAvailabilityViolation, hasIntegrityViolation));
                        }
                        catch
                        {
                            errorMessage += $"Угроза №{row - 2} имеет неверный формат!\n";
                        }
                    }

                    if (errorMessage != "")
                        MessageBox.Show(errorMessage + "Попробуйте обновить базу!", "Внимание", MessageBoxButton.OK, MessageBoxImage.Warning);

                    return threats;
                }
            }
        }

        private void PagesCountCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (pagination == null)
                return;

            pagination.DisplayedCount = int.Parse(((sender as ComboBox).SelectedItem as ComboBoxItem).Content.ToString());
        }

        private void UpdatePaginationGUI()
        {
            PaginationTB.Text = pagination.ToString();

            FirstPageBtn.IsEnabled = PreviousPageBtn.IsEnabled = (pagination.CurrentPage > 1);
            LastPageBtn.IsEnabled = NextPageBtn.IsEnabled = (pagination.CurrentPage < pagination.PagesCount);
        }

        private void ThreatsDG_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if ((sender as DataGrid).SelectedItem is Threat threat)
            {
                var threatInfoWindow = new ThreatInfoWindow(threat);
                threatInfoWindow.Show();
            }
        }

        private async void UpdateBtn_Click(object sender, RoutedEventArgs e)
        {
            UpdateBtn.IsEnabled = false;
            string path = "thrlist_new.xlsx";
            if (File.Exists(path))
            {
                try
                {
                    File.Delete(path);
                }
                catch
                {
                    // я проверял этот алгоритм, когда у файла поменялись права и когда он был открыт.
                    int i = 1;
                    while (File.Exists($"thrlist_new{i}.xlsx"))
                    {
                        try
                        {
                            File.Delete($"thrlist_new{i}.xlsx");
                            break;
                        }
                        catch
                        {
                            i++;
                        }
                    }
                    path = $"thrlist_new{i}.xlsx";
                }
            }

            var newThreats = await DownloadThreatsFromInternet(threatsUrl, path);

            var updateReport = new UpdateReport(pagination.GetThreats(), newThreats);
            if (updateReport.HaveChanges)
            {
                var updateReportWindow = new UpdateReportWindow(updateReport);
                updateReportWindow.ShowDialog();

                try
                {
                    if (File.Exists(threatsPath))
                        File.Delete(threatsPath);
                    
                    File.Move(path, threatsPath);

                    pagination.UpdateThreats(newThreats, pagination.DisplayedCount);
                }
                catch
                {
                    MessageBox.Show($"Не удалось обновить файл с базой угроз!\nДля корректной работы с программой Вам необходимо самостоятельно удалить файл {threatsPath}, находящийся в папке с программой!", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                if(File.Exists(path))
                {
                    try
                    {
                        File.Delete(path);
                    }
                    catch { }
                }
                MessageBox.Show("База угроз находится в актуальном состоянии!", "Результат обновления", MessageBoxButton.OK, MessageBoxImage.Information);
            }

            UpdateBtn.IsEnabled = true;
        }

        private async Task<List<Threat>> DownloadThreatsFromInternet(string url, string path)
        {
            if (!await Task.Run(new Func<bool>(() => DownloadFile(url, path))))
            {
                MessageBox.Show("Ошибка при попытки скачивания базы угроз!\nПроверьте наличие подключения к сети Интернет и попробуйте снова!", "Не удалось скачать базу угроз!", MessageBoxButton.OK, MessageBoxImage.Error);
                return new List<Threat>();
            }

            return await Task.Run(new Func<List<Threat>>(() => ParseThreats(path)));
        }
    }
}
