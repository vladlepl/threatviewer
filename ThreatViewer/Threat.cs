﻿using System.Collections.Generic;
using System.Linq;

namespace ThreatViewer
{
    public class Threat
    {
        public int Id { get; private set; }
        public string Name { get; private set; }
        public string Description { get; private set; }
        public string[] Sources { get; private set; }
        public string[] Objects { get; private set; }
        public bool HasConfidentialityViolation { get; private set; }
        public bool HasAvailabilityViolation { get; private set; }
        public bool HasIntegrityViolation { get; private set; }
        
        public string SourcesStr { get; private set; }
        public string ObjectsStr { get; private set; }
        
        private static string[] objectExceptions;
        static Threat()
        {
            // суть в том, что у некоторых объектов воздействия есть запятые, не являющиеся разделителями объектов
            // примеры ниже (взятые из таблицы) как раз демонстрируют это
            // поэтому приходится так убого это обрабатывать
            // P.S. писал этот код в 2 часа ночи, выглядит мегаупорото) (особенно метод SplitStringIntoArray)
            objectExceptions = new string[]
            {
                "микропрограммное, системное и прикладное программное обеспечение",
                "мобильные устройства (аппаратное устройство, программное обеспечение)",
                "мобильное устройство и запущенные на  нем приложения (программное обеспечение, аппаратное устройство)",
                "система управления доступом, встроенная в операционную систему компьютера (программное обеспечение)",
                "информация, хранящаяся на компьютере во временных файлах (программное обеспечение)",
                "технические средства воздушного кондиционирования, включая трубопроводные системы для циркуляции охлаждённого воздуха в ЦОД",
                "облачная инфраструктура, созданная с использованием технологий виртуализации",
                "информационная система, иммигрированная в облако",
                "системное программное обеспечение, использующее реестр",
                "виртуальные устройства хранения, обработки и передачи данных"
            };
        }

        public Threat(int id, string name, string description, string sources, string objects, bool hasConfidentialityViolation, bool hasAvailabilityViolation, bool hasIntegrityViolation)
        {
            this.Id = id;
            this.Name = name;
            this.Description = description;
            this.SourcesStr = sources;
            this.Sources = SplitStringIntoArray(sources);
            this.ObjectsStr = objects;
            this.Objects = SplitStringIntoArray(objects, exceptions: objectExceptions);
            this.HasConfidentialityViolation = hasConfidentialityViolation;
            this.HasAvailabilityViolation = hasAvailabilityViolation;
            this.HasIntegrityViolation = hasIntegrityViolation;
        }

        /// <summary>
        /// Метод, формирующий список из строки по заданному разделителю
        /// </summary>
        /// <param name="str">Строка, содержащая список некоторых значений</param>
        /// <param name="separator">Разделитель</param>
        /// <param name="exceptions">Исключения</param>
        /// <returns>Массив значений (каждое значение начинается с заглавной буквы)</returns>
        private static string[] SplitStringIntoArray(string str, char separator = ',', IEnumerable<string> exceptions = null)
        {
            List<string> array;
            if (exceptions == null)
                array = new List<string>(str.Split(separator));
            else
            {
                array = new List<string>();
                str = str.ToLower();
                
                foreach (var exception in exceptions)
                {
                    var lowerException = exception.ToLower();
                    if (str.Contains(lowerException))
                    {
                        str = str.Replace(lowerException, "");
                        array.Add(exception);
                    }
                }

                array.AddRange(str.Split(separator));
            }

            for (int i = 0; i < array.Count; i++)
                array[i] = array[i].Trim();

            array = array.Where(x => x.Length > 0).ToList();

            if (array.Count == 1 && array[0].Length == 0)
                return null;

            for (int i = 0; i < array.Count; i++)
            {
                var temp = array[i];
                array[i] = temp[0].ToString().ToUpper();
                if (temp.Length > 1)
                    array[i] += temp.Substring(1, temp.Length - 1);
            }

            return array.ToArray();
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            
            if (obj is Threat threat)
                return this.Id == threat.Id &&
                    this.Name == threat.Name &&
                    this.Description == threat.Description &&
                    this.SourcesStr == threat.SourcesStr &&
                    this.ObjectsStr == threat.ObjectsStr &&
                    this.HasConfidentialityViolation == threat.HasConfidentialityViolation &&
                    this.HasAvailabilityViolation == threat.HasAvailabilityViolation &&
                    this.HasIntegrityViolation == threat.HasIntegrityViolation;
            else
                return false;
        }

        public override int GetHashCode()
        {
            var hashCode = 1251472809;
            hashCode = hashCode * -1521134295 + Id.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Name);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Description);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(SourcesStr);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(ObjectsStr);
            hashCode = hashCode * -1521134295 + HasConfidentialityViolation.GetHashCode();
            hashCode = hashCode * -1521134295 + HasAvailabilityViolation.GetHashCode();
            hashCode = hashCode * -1521134295 + HasIntegrityViolation.GetHashCode();
            return hashCode;
        }

        public static Threat Copy(Threat t)
        {
            return new Threat(t.Id, t.Name, t.Description, t.SourcesStr, t.ObjectsStr, t.HasConfidentialityViolation, t.HasAvailabilityViolation, t.HasIntegrityViolation);
        }
    }
}
