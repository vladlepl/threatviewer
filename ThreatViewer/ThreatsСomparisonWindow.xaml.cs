﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace ThreatViewer
{
    /// <summary>
    /// Interaction logic for ThreatsСomparisonWindow.xaml
    /// </summary>
    public partial class ThreatsСomparisonWindow : Window
    {
        public Threat OldThreat { get; private set; }
        public Threat NewThreat { get; private set; }

        public ThreatsСomparisonWindow(Threat oldThreat, Threat newThreat)
        {
            InitializeComponent();

            CloseWindowBtn.Click += delegate (object sender, RoutedEventArgs e) { this.Close(); };

            this.OldThreat = oldThreat;
            this.NewThreat = newThreat;

            this.Title += $" УБИ.{NewThreat.Id}";
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            IdTB.Text = $"УБИ.{NewThreat.Id}";

            if (OldThreat.Name != NewThreat.Name)
            {
                OldNameTB.Text = OldThreat.Name;
                NewNameTB.Text = NewThreat.Name;
            }
            else
            {
                NameWithoutChangesTB.Visibility = Visibility.Visible;
                NameChangesRow.Height = new GridLength(0, GridUnitType.Pixel);

                NameTB.Visibility = Visibility.Visible;
                NameTB.Text = NewThreat.Name;
            }

            if (OldThreat.Description != NewThreat.Description)
            {
                OldDescriptionTB.Text = OldThreat.Description;
                NewDescriptionTB.Text = NewThreat.Description;
            }
            else
            {
                DescriptionWithoutChangesTB.Visibility = Visibility.Visible;
                DescriptionChangesRow.Height = new GridLength(0, GridUnitType.Pixel);

                DescriptionTB.Visibility = Visibility.Visible;
                DescriptionTB.Text = NewThreat.Description;
            }

            if (OldThreat.SourcesStr != NewThreat.SourcesStr)
            {
                OldSourcesListBox.ItemsSource = GetItemsSource(OldThreat.Sources);
                NewSourcesListBox.ItemsSource = GetItemsSource(NewThreat.Sources);
            }
            else
            {
                SourcesWithoutChangesTB.Visibility = Visibility.Visible;
                SourcesChangesRow.Height = new GridLength(0, GridUnitType.Pixel);

                SourcesListBox.Visibility = Visibility.Visible;
                SourcesListBox.ItemsSource = GetItemsSource(NewThreat.Sources);
            }

            if (OldThreat.ObjectsStr != NewThreat.ObjectsStr)
            {
                OldObjectsListBox.ItemsSource = GetItemsSource(OldThreat.Objects);
                NewObjectsListBox.ItemsSource = GetItemsSource(NewThreat.Objects);
            }
            else
            {
                ObjectsWithoutChangesTB.Visibility = Visibility.Visible;
                ObjectsChangesRow.Height = new GridLength(0, GridUnitType.Pixel);

                ObjectsListBox.Visibility = Visibility.Visible;
                ObjectsListBox.ItemsSource = GetItemsSource(NewThreat.Objects);
            }

            if (OldThreat.HasAvailabilityViolation != NewThreat.HasAvailabilityViolation ||
                OldThreat.HasConfidentialityViolation != NewThreat.HasConfidentialityViolation ||
                OldThreat.HasIntegrityViolation != NewThreat.HasIntegrityViolation)
            {
                OldViolationListBox.ItemsSource = GetItemsSource(GetViolations(OldThreat));
                NewViolationListBox.ItemsSource = GetItemsSource(GetViolations(NewThreat));
            }
            else
            {
                ViolationWithoutChangesTB.Visibility = Visibility.Visible;
                ViolationChangesRow.Height = new GridLength(0, GridUnitType.Pixel);

                ViolationListBox.Visibility = Visibility.Visible;
                ViolationListBox.ItemsSource = GetItemsSource(GetViolations(NewThreat));
            }
        }

        private static List<string> GetViolations(Threat threat)
        {
            var violations = new List<string>();
            if (threat.HasConfidentialityViolation)
                violations.Add("Угроза конфиденциальности");
            if (threat.HasAvailabilityViolation)
                violations.Add("Угроза доступности");
            if (threat.HasIntegrityViolation)
                violations.Add("Угроза целостности");

            return violations;
        }

        private static IEnumerable<string> GetItemsSource(IEnumerable<string> items)
        {
            if (items == null || items.Count() == 0)
                return new string[] { "Отсутствуют" };

            return items;
        }
    }
}
