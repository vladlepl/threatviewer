﻿using System.Windows;
using System.Windows.Input;

namespace ThreatViewer
{
    /// <summary>
    /// Interaction logic for UpdateReportWindow.xaml
    /// </summary>
    public partial class UpdateReportWindow : Window
    {
        UpdateReport updateReport { get; set; }

        public UpdateReportWindow(UpdateReport updateReport)
        {
            InitializeComponent();
         
            this.updateReport = updateReport;

            CloseWindowBtn.Click += delegate (object sender, RoutedEventArgs e) { this.Close(); };

            AddedThreatsLBI.Selected += AddedThreatsLBI_Selected;
            RemovedThreatsLBI.Selected += RemovedThreatsLBI_Selected;
            EditedThreatsLBI.Selected += EditedThreatsLBI_Selected;
        }


        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            AddedThreatsTB.Text = updateReport.AddedThreats.Count.ToString();
            RemovedThreatsTB.Text = updateReport.RemovedThreats.Count.ToString();
            EditedThreatsTB.Text = updateReport.EditedThreats.Count.ToString();

            EditedThreatsLBI.IsSelected = true;
        }

        private void ThreatsDG_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (ReportLB.SelectedItem == AddedThreatsLBI || ReportLB.SelectedItem == RemovedThreatsLBI)
            {
                if (ThreatsDG.SelectedItem != null)
                {
                    var threatInfoWindow = new ThreatInfoWindow(ThreatsDG.SelectedItem as Threat);
                    threatInfoWindow.Show();
                }
            }
            else
            {
                if (ThreatsDG.SelectedItem != null)
                {
                    var threat = ThreatsDG.SelectedItem as Threat;
                    var threatInfoWindow = new ThreatsСomparisonWindow(threat, updateReport.EditedThreats[threat]);
                    threatInfoWindow.Show();
                }
            }
        }

        private void AddedThreatsLBI_Selected(object sender, RoutedEventArgs e)
        {
            ThreatsDG.ItemsSource = updateReport.AddedThreats;
            ThreatsDG.Items.Refresh();
        }

        private void EditedThreatsLBI_Selected(object sender, RoutedEventArgs e)
        {
            ThreatsDG.ItemsSource = updateReport.EditedThreats.Keys;
            ThreatsDG.Items.Refresh();
        }

        private void RemovedThreatsLBI_Selected(object sender, RoutedEventArgs e)
        {
            ThreatsDG.ItemsSource = updateReport.RemovedThreats;
            ThreatsDG.Items.Refresh();
        }
    }
}
