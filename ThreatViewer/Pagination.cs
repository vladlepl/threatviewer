﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace ThreatViewer
{
    public class Pagination
    {
        private List<Threat> threats;
        public ObservableCollection<Threat> DisplayedThreats { get; private set; }

        public delegate void ThreatsHandler();
        public event ThreatsHandler DisplayedThreatsChanged;

        private int displayedCount = 15;
        public int DisplayedCount
        {
            get => displayedCount;
            set
            {
                displayedCount = value;
                CurrentPage = 1;
            }
        }

        public int PagesCount
        {
            get
            {
                if (threats.Count % DisplayedCount == 0)
                    return threats.Count / DisplayedCount;
                else
                    return threats.Count / DisplayedCount + 1;
            }
        }

        private int currentPage = 0;
        public int CurrentPage
        {
            get => currentPage;
            private set
            {
                if (value < 1)
                    currentPage = 1;
                else if (value > PagesCount)
                    currentPage = PagesCount;
                else
                    currentPage = value;

                ChangeDisplayedList();
            }
        }

        public Pagination()
        {
            threats = new List<Threat>();
            DisplayedThreats = new ObservableCollection<Threat>();
        }

        public Pagination(List<Threat> threats, int displayedCount) : this()
        {
            UpdateThreats(threats, displayedCount);
        }

        public void UpdateThreats(List<Threat> threats, int displayedCount = 15)
        {
            this.threats.Clear();

            foreach (var threat in threats)
                this.threats.Add(Threat.Copy(threat));

            DisplayedCount = displayedCount;
        }

        public List<Threat> GetThreats()
        {
            var threatsListCopy = new List<Threat>(threats.Count);

            foreach (var threat in threats)
                 threatsListCopy.Add(Threat.Copy(threat));

            return threatsListCopy;
        }

        public void MoveNextPage()
        {
            CurrentPage++;
        }

        public void MovePreviousPage()
        {
            CurrentPage--;
        }

        public void MoveFirstPage()
        {
            CurrentPage = 1;
        }

        public void MoveLastPage()
        {
            CurrentPage = PagesCount;
        }

        private void ChangeDisplayedList()
        {
            DisplayedThreats.Clear();

            for (int i = (CurrentPage - 1) * DisplayedCount; i < (CurrentPage) * DisplayedCount && i < threats.Count && i >= 0; i++)
                DisplayedThreats.Add(threats[i]);

            DisplayedThreatsChanged?.Invoke();
        }

        public override string ToString()
        {
            return $"{CurrentPage} из {PagesCount}";
        }
    }
}
