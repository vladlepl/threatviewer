﻿using System.Collections.Generic;
using System.Linq;

namespace ThreatViewer
{
    public class UpdateReport
    {
        public List<Threat> AddedThreats { get; private set; }
        public List<Threat> RemovedThreats { get; private set; }
        public Dictionary<Threat, Threat> EditedThreats { get; private set; }

        public bool HaveChanges => AddedThreats.Count != 0 || RemovedThreats.Count != 0 || EditedThreats.Count != 0;

        public UpdateReport(List<Threat> oldThreats, List<Threat> newThreats)
        {
            var oldIds = oldThreats.Select(x => x.Id);
            var newIds = newThreats.Select(x => x.Id);

            var removedIds = oldIds.Except(newIds);
            var addedIds = newIds.Except(oldIds);
            AddedThreats = new List<Threat>(newThreats.Where(x => addedIds.Contains(x.Id)));
            RemovedThreats = new List<Threat>(oldThreats.Where(x => removedIds.Contains(x.Id)));

            EditedThreats = new Dictionary<Threat, Threat>();
            var sameIds = oldIds.Intersect(newIds);
            var oldSameIdThreats = new List<Threat>(oldThreats.Where(x => sameIds.Contains(x.Id)).OrderBy(x => x.Id));
            var newSameIdThreats = new List<Threat>(newThreats.Where(x => sameIds.Contains(x.Id)).OrderBy(x => x.Id));

            for(int i = 0; i < sameIds.Count(); i++)
                if (!oldSameIdThreats[i].Equals(newSameIdThreats[i]))
                    EditedThreats.Add(newSameIdThreats[i], oldSameIdThreats[i]);
        }
    }
}
