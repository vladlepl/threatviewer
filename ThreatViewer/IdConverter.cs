﻿using System;
using System.Globalization;
using System.Windows.Controls;
using System.Windows.Data;

namespace ThreatViewer
{
    public class IdConverter : IValueConverter
    {
        public object Convert(object value, Type TargetType, object parameter, CultureInfo culture)
        {
            return $"УБИ.{value}";
        }
        public object ConvertBack(object value, Type TargetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
