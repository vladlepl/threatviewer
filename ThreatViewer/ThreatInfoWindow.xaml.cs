﻿using System.Collections.Generic;
using System.Windows;

namespace ThreatViewer
{
    /// <summary>
    /// Interaction logic for ThreatInfoWindow.xaml
    /// </summary>
    public partial class ThreatInfoWindow : Window
    {
        private Threat threat;

        public ThreatInfoWindow(Threat threat)
        {
            InitializeComponent();
            
            this.threat = threat;
            this.Title += $" УБИ.{this.threat.Id}";
            
            CloseWindowBtn.Click += delegate (object sender, RoutedEventArgs e) { this.Close(); };
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            IdTB.Text = $"УБИ.{threat.Id}";
            NameTB.Text = threat.Name;
            DescriptionTB.Text = threat.Description;

            if (threat.Sources != null && threat.Sources.Length > 0)
                SourcesListBox.ItemsSource = threat.Sources;
            else
                SourcesListBox.ItemsSource = new string[] { "Отсутствуют" };

            if (threat.Objects != null && threat.Sources.Length > 0)
                ObjectsListBox.ItemsSource = threat.Objects;
            else
                ObjectsListBox.ItemsSource = new string[] { "Отсутствуют" };

            var violations = new List<string>();
            if (threat.HasConfidentialityViolation)
                violations.Add("Угроза конфиденциальности");
            if (threat.HasAvailabilityViolation)
                violations.Add("Угроза доступности");
            if (threat.HasIntegrityViolation)
                violations.Add("Угроза целостности");

            if (violations.Count > 0)
                ViolationListBox.ItemsSource = violations;
            else
                ViolationListBox.ItemsSource = new string[] { "Отсутствуют" };
        }
    }
}
